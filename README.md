# ed-oscad

A CAD model building framework.

## Overview

This is a make based framework for building CAD models which have been scripted
using [OpenSCAD](https://openscad.org).

The models themselves are intended to be clones of git repositories residing
in the "models" directory.

If a model declares itself to be dependent upon a library then that library is
automatically installed in the "lib" directory.

OpenSCAD is installed in a "bin" directory so that there is no dependence upon
what is installed in the system directories.

The build can produce STLs for 3D printing and PNGs for documentation.

The concept of assemblies and subassemblies is supported. A model is expected
to have the following directory structure:

- **model** - The directory containing the model files. Each model directory
                must have a unique name.
  - **inc** - Included files. Typically used for parametric values.
  - **sketches** - 2D parts sketches. These should be designed to be extrudable
                    in a parts file.
  - **parts** - Individual parts. Single file per part.
  - **prints** - Collections of 3D printable parts. Again, single file per part.
  - **assemblies** - Collections of parts, prints, and subassemblies to produce
                 the complete model. Each assembly file should be prefixed with
                 a pattern to allow use of the ASM option on the make command
                 line.
    Note that a model can have multiple assemblies.

All other directories are at the discretion of the model designer.

The OpenSCAD search path, OPENSCADPATH, is set to point to the **model**
directory. Includes in the scripts should use the relative path to the
included file.

## How do I get set up?

Clone this repository and then clone a model in to the **model** directory and
run "make MODEL=model".

Running "make help" will display usage information.

    Usage: "make MODEL=<model> [ASM=<asmprefix>] [<target>]"
    MODEL=<model> The model to build. This is used as a prefix to the directories
        where the model scripts are located and is required.
    ASM=<asmprefix> This is the assembly prefix. The assemblies directory is scanned
        for files having this prefix and only those files are processed.
    <target>A single target.
        Possible targets:
        all     All assembly files are processed and .stl and .png files
                produced.
        stl     Only the stl files are generated.
        png     Only the png files are generated.
        gui     Run the OpenSCAD GUI for each of the assembly files.
        stopgui Shutdown the OpenSCAD GUI.
        clean   Remove the dependency files and the output files.
        help    Display this help message.
        show-<variable>
                This is a special target which can be used to display
                any makefile variable and exit.
        <asmfile>.stl
                Use this target to process a single assembly file. NOTE: The
                ASM command line variable can also be used for this purpose.
