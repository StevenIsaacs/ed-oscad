# The "models" directory

This directory is where SCAD models are intended to reside. These should be
git repositories cloned individually.
