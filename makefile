#+
# This make file segment is designed to be used either stand-alone or
# as an Apis project in the Apis SDE directory.
#-
ifndef project_mk
project_mk = ed-oscad

$(info $(lastword ${MAKEFILE_LIST}): ${project_mk})

$(info Goal: ${MAKECMDGOALS})

project_dir = $(dir $(realpath $(firstword ${MAKEFILE_LIST})))
$(info project_dir: ${project_dir})

ifeq ($(shell grep WSL /proc/version > /dev/null; echo $$?),0)
  Platform = Microsoft
endif

export EdOscadUsage
help:
	@echo "$$EdOscadUsage" | less

# Use MODEL_DIR on the make command line to specify which model to build.
# e.g. make MODEL_DIR=<model path>
$(info MODEL_DIR=${MODEL_DIR})
ifeq (${MODEL_DIR},)
  # Where the CAD model resides.
  ifeq (${MODELS_DIR},)
    MODELS_DIR = $(realpath ${project_dir}models)
  endif
  ifneq (${MODEL},)
    MODEL_DIR = $(realpath ${MODELS_DIR}/${MODEL})
    # If MODEL_DIR is still null then it doesn't exist.
	ifeq (${MODEL_DIR},)
      ifeq (${MAKECMDGOALS},init)
        MODEL_DIR = models/${MODEL}
        $(info Initializing: ${MODEL_DIR})
      else
        $(error ${MODEL} directory does not exist)
      endif
    endif
  else
    $(info ${EdOscadUsage})
    $(error MODEL or MODEL_DIR is a required argument.)
  endif
endif

$(info Building ${MODEL_DIR})

# Where tools are installed.
OSC_BIN_DIR = $(realpath ${project_dir}bin)
# Included or imported files.
OSC_INC_DIR = ${MODEL_DIR}/inc
# 2D sketches.
OSC_DRAW_DIR = ${MODEL_DIR}/drawings
# Parts.
OSC_PART_DIR = ${MODEL_DIR}/parts
# Printable parts.
OSC_PRINT_DIR = ${MODEL_DIR}/prints
# Assemblies and subassemblies of printable pars.
OSC_ASM_DIR = ${MODEL_DIR}/assemblies
# Off the shelf components (downloaded from the net).
OSC_OTS_DIR = ${MODEL_DIR}/ots

# Output directories.
OSC_BUILD_DIR = ${MODEL_DIR}/build
OSC_DOC_DIR = ${OSC_BUILD_DIR}/doc
OSC_STL_DIR = ${OSC_BUILD_DIR}/stl
OSC_PNG_DIR = ${OSC_BUILD_DIR}/png

# Where common libraries reside.
OSC_LIB_DIR = $(realpath ${project_dir}lib)

OPENSCAD_VERSION = 2021.01-x86_64
OPENSCAD_APP = OpenSCAD-${OPENSCAD_VERSION}.AppImage
ifeq (${Platform},Microsoft)
  OPENSCAD_BIN = squashfs-root/AppRun
else
  OPENSCAD_BIN = ${OPENSCAD_APP}
endif
OPENSCAD_URL = https://files.openscad.org/${OPENSCAD_APP}
OPENSCAD_GUI = ${OSC_BIN_DIR}/${OPENSCAD_APP}
OPENSCAD_CLI = ${OSC_BIN_DIR}/${OPENSCAD_BIN}
OPENSCADPATH = ${MODEL_DIR}:${OSC_LIB_DIR}

# MODEL_LIBS and MODEL_DEPS are optionally defined in model.mk.
include $(wildcard ${MODEL_DIR}/model.mk)
ModelDeps = ${MODEL_LIBS} ${MODEL_DEPS}

# OpenSCAD source files.
sc_drawing_files = $(wildcard ${OSC_DRAW_DIR}/*.scad)
sc_printable_files = $(wildcard ${OSC_PRINT_DIR}/*.scad)
sc_assembly_files = $(wildcard ${OSC_ASM_DIR}/${ASM}*.scad)
sc_model_files = ${sc_drawing_files} ${sc_printable_files} ${sc_assembly_files}

sc_stl_files = $(foreach file, ${sc_printable_files}, ${OSC_STL_DIR}/$(basename $(notdir $(file))).stl)
sc_png_files = $(foreach file, ${sc_model_files}, ${OSC_PNG_DIR}/$(basename $(notdir $(file))).png)

printable_files = ${sc_printable_files}
model_files = ${sc_model_files}

# SolidPython source files.
sp_parts_files = $(wildcard ${OSC_PART_DIR}/*.py)
sp_drawing_files = $(wildcard ${OSC_DRAW_DIR}/*.py)
sp_printable_files = $(wildcard ${OSC_PRINT_DIR}/*.py)
sp_assembly_files = $(wildcard ${OSC_ASM_DIR}/${ASM}*.py)
sp_import_files = $(wildcard ${OSC_INC_DIR}/*.py)
sp_model_files = ${sp_drawing_files} ${sp_printable_files} ${sp_assembly_files}

sp_dep_files = $(foreach file, ${sp_model_files}, ${OSC_BUILD_DIR}/$(basename $(notdir $(file))).pdeps)
sp_stl_files = $(foreach file, ${sp_printable_files}, ${OSC_STL_DIR}/$(basename $(notdir $(file))).stl)
ifneq (${Platform},Microsoft)
sp_png_files = $(foreach file, ${sp_model_files}, ${OSC_PNG_DIR}/$(basename $(notdir $(file))).png)
endif
sp_scad_files = $(foreach file, ${sp_stl_files}, ${OSC_BUILD_DIR}/$(notdir $(file)).scad)
sp_doc_files = $(foreach file, \
	${sp_model_files} ${sp_import_files} ${sp_parts_files}, \
	${OSC_DOC_DIR}/$(basename $(notdir $(file))).md)

printable_files += ${sp_printable_files}
model_files += ${sp_model_files}

all: ${OPENSCAD_CLI} ${ModelDeps} \
  ${sc_stl_files} \
  ${sc_png_files} \
  ${sp_stl_files} \
  ${sp_png_files} \
  ${sp_doc_files}

#+
# If python files are present then assume SolidPython in a virtual Platform
# is needed.
#-
$(info Using SolidPython)

SolidPythonPath = ${MODEL_DIR}:${OSC_LIB_DIR}

#+
# Python virtual Platform requirements.
#-
SpPythonVersion = 3.8
SpVirtualEnvDir = sp_venv
SpPythonEnvFile = ${MODEL_DIR}/.env

SpVenvPackageDir = ${SpVirtualEnvDir}/lib/python${SpPythonVersion}/site-packages

VenvRequirements = \
  ${SpVenvPackageDir}/ptvsd/__init__.py \
  ${SpVenvPackageDir}/flake8/__init__.py \
  ${SpVenvPackageDir}/pdoc/__init__.py \
  ${SpVenvPackageDir}/configparser.py \
  ${SpVenvPackageDir}/configobj.py \
  ${SpVenvPackageDir}/cmd2/cmd2.py \
  ${SpVenvPackageDir}/numpy/__init__.py

${SpVirtualEnvDir}/bin/python3:
	python3 -m venv --copies ./${SpVirtualEnvDir}

define SpInstallPythonPackage =
$(info ++++++++++++)
$(info SpInstallPythonPackage $1)
	( \
	  . ${SpVirtualEnvDir}/bin/activate; \
	  pip3 install $1; \
	)
endef

${SpVenvPackageDir}/ptvsd/__init__.py:
	$(call SpInstallPythonPackage, ptvsd)

${SpVenvPackageDir}/flake8/__init__.py:
	$(call SpInstallPythonPackage, flake8)

${SpVenvPackageDir}/pdoc/__init__.py:
	$(call SpInstallPythonPackage, pdoc3)

${SpVenvPackageDir}/configparser.py:
	$(call SpInstallPythonPackage, configparser)

${SpVenvPackageDir}/configobj.py:
	$(call SpInstallPythonPackage, configobj)

${SpVenvPackageDir}/cmd2/cmd2.py:
	$(call SpInstallPythonPackage, cmd2)

${SpVenvPackageDir}/numpy/__init__.py:
	$(call SpInstallPythonPackage, numpy)

SolidPython = ${SpVenvPackageDir}/solid/__init__.py
${SolidPython}: \
  ${SpVirtualEnvDir}/bin/python3 \
  ${VenvRequirements}
	$(call SpInstallPythonPackage, solidpython)

${SpPythonEnvFile}:
	echo "PYTHONPATH=${SolidPythonPath}" > ${SpPythonEnvFile}

python: ${SolidPython} ${SpPythonEnvFile}
	( \
	. ${SpVirtualEnvDir}/bin/activate; \
	cd ${MODEL_DIR}; \
	OPENSCADPATH=${OPENSCADPATH} \
	PYTHONPATH=${SolidPythonPath} python; \
	deactivate; \
	)

PDOC = \
	. ${SpVirtualEnvDir}/bin/activate; \
	cd ${MODEL_DIR}; \
	python -B -m pdoc --force -o $(dir $@) $<; \
	deactivate

stl_files = $(foreach file, ${printable_files}, \
  ${OSC_STL_DIR}/$(basename $(notdir $(file))).stl)
ifneq (${Platform},Microsoft)
png_files = $(foreach file, ${model_files}, \
  ${OSC_PNG_DIR}/$(basename $(notdir $(file))).png)
endif

$(info Libraries $(wildcard ${OSC_LIB_DIR}/*.mk))
include $(wildcard ${OSC_LIB_DIR}/*.mk)

# This assumes downloading an executable binary (e.g. Appimage).
${OPENSCAD_GUI}:
	wget -O $@ ${OPENSCAD_URL}
	touch $@
	chmod +x $@

ifeq (${Platform},Microsoft)
${OPENSCAD_CLI}: ${OPENSCAD_GUI}
	cd $(<D); \
	${OPENSCAD_GUI} --appimage-extract
endif

stl: ${OPENSCAD_CLI} ${ModelDeps} ${stl_files}

# See if running in WSL. If so The OpenSCAD GUI can't be started without
# an X server running which is outside the scope of this project.
ifneq (${Platform},Microsoft)
png: ${OPENSCAD_CLI} ${ModelDeps} ${png_files}

gui: ${OPENSCAD_GUI} ${ModelDeps} ${stl_files} ${sp_scad_files}
	 OPENSCADPATH=${OPENSCADPATH} ${OPENSCAD_CLI} \
	 ${sc_model_files} ${sp_scad_files} &
	 @echo "${OPENSCAD_CLI} started and has process ID: `pidof ${OPENSCAD_CLI}`"
else
gui:
	$(error Cannot run the OpenSCAD GUI in a WSL Platform.)
endif

docs: ${SolidPython} ${sp_doc_files}

define Readme
# Describe the model here.

Model...

## Recommended directory names.

- model.mk      Model dependencies.
- ots           Model specific off the shelf components.
- inc           File shared with model scripts.
- parts         Files describing parts used for prints and assemblies.
- prints        Files describing 3D printed parts.
- assemblies    Files describing assemblies of parts and their relationships.
- drawings      Files describing dimensioned drawings.
endef

# This will fail if the model already exists.
export Readme
init:
	mkdir ${MODEL_DIR}
	cd ${MODEL_DIR} && git init
	@echo "# Model dependencies." > ${MODEL_DIR}/model.mk
	@echo "$$Readme" > ${MODEL_DIR}/README.md

clean:
	rm -rf ${OSC_BUILD_DIR}

OPENSCAD_STL = OPENSCADPATH=${OPENSCADPATH} \
	${OPENSCAD_CLI} -m ${MAKE} -o ${OSC_STL_DIR}/$(notdir $@) \
	-d ${OSC_BUILD_DIR}/$(notdir $@).deps $<

OPENSCAD_PNG = OPENSCADPATH=${OPENSCADPATH} \
	${OPENSCAD_CLI} -m ${MAKE} -o ${OSC_PNG_DIR}/$(notdir $@) \
	-d ${OSC_BUILD_DIR}/$(notdir $@).deps $<

SolidPython_STL = \
	. ${SpVirtualEnvDir}/bin/activate; \
	cd ${MODEL_DIR} && \
	OPENSCADPATH=${OPENSCADPATH} \
	PYTHONPATH=${SolidPythonPath} \
	python -B $< ${OSC_BUILD_DIR}/$(notdir $@).scad && \
	deactivate && \
	${OPENSCAD_CLI} -m ${MAKE} -o ${OSC_STL_DIR}/$(notdir $@) \
	-d ${OSC_BUILD_DIR}/$(notdir $@).deps ${OSC_BUILD_DIR}/$(notdir $@).scad

SolidPython_PNG = \
	. ${SpVirtualEnvDir}/bin/activate; \
	cd ${MODEL_DIR} && \
	OPENSCADPATH=${OPENSCADPATH} \
	PYTHONPATH=${SolidPythonPath} \
	python -B $< ${OSC_BUILD_DIR}/$(notdir $@).scad && \
	deactivate && \
	${OPENSCAD_CLI} -m ${MAKE} -o ${OSC_PNG_DIR}/$(notdir $@) \
	  -d ${OSC_BUILD_DIR}/$(notdir $@).deps \
	  ${OSC_BUILD_DIR}/$(notdir $@).scad

PDEPS = \
	. ${SpVirtualEnvDir}/bin/activate; \
	cd ${MODEL_DIR} && \
	PYTHONPATH=${SolidPythonPath} \
	python -B ${project_dir}/helpers/pdeps.py $< > $@ && \
	deactivate

include $(wildcard ${OSC_BUILD_DIR}/*.deps)
# This will cause make to run the utility to generate the dependencies.
-include ${sp_dep_files}

# Display the value of any variable.
show-%:
	@echo '$*=$($*)'

# Drawings
ifneq (${MAKECMDGOALS},clean)
ifneq (${MAKECMDGOALS},help)
$(info Defining pattern rules)
${OSC_PNG_DIR}/%.png: ${OSC_DRAW_DIR}/%.scad
	mkdir -p ${OSC_BUILD_DIR}
	mkdir -p $(dir $@)
	${OPENSCAD_PNG}

# 3D printables
${OSC_STL_DIR}/%.stl: ${OSC_PRINT_DIR}/%.scad
	mkdir -p ${OSC_BUILD_DIR}
	mkdir -p $(dir $@)
	${OPENSCAD_STL}

${OSC_PNG_DIR}/%.png: ${OSC_PRINT_DIR}/%.scad
	mkdir -p ${OSC_BUILD_DIR}
	mkdir -p $(dir $@)
	${OPENSCAD_PNG}

# Assemblies
${OSC_STL_DIR}/%.stl: ${OSC_ASM_DIR}/%.scad
	mkdir -p ${OSC_BUILD_DIR}
	mkdir -p $(dir $@)
	${OPENSCAD_STL}

${OSC_PNG_DIR}/%.png: ${OSC_ASM_DIR}/%.scad
	mkdir -p ${OSC_BUILD_DIR}
	mkdir -p $(dir $@)
	${OPENSCAD_PNG}

# For SolidPython intermediates and docs.

# Dependencies
${OSC_BUILD_DIR}/%.pdeps: \
  ${OSC_DRAW_DIR}/%.py ${SolidPython} ${SpPythonEnvFile}
	mkdir -p $(dir $@)
	${PDEPS}

${OSC_BUILD_DIR}/%.pdeps: \
  ${OSC_PRINT_DIR}/%.py ${SolidPython} ${SpPythonEnvFile}
	mkdir -p $(dir $@)
	${PDEPS}

${OSC_BUILD_DIR}/%.pdeps: \
  ${OSC_ASM_DIR}/%.py ${SolidPython} ${SpPythonEnvFile}
	mkdir -p $(dir $@)
	${PDEPS}

# Drawings.
${OSC_PNG_DIR}/%.png: \
  ${OSC_DRAW_DIR}/%.py ${SolidPython} ${SpPythonEnvFile}
	mkdir -p ${OSC_BUILD_DIR}
	mkdir -p $(dir $@)
	${SolidPython_PNG}

# 3D printables.
${OSC_STL_DIR}/%.stl: \
  ${OSC_PRINT_DIR}/%.py ${SolidPython} ${SpPythonEnvFile}
	mkdir -p ${OSC_BUILD_DIR}
	mkdir -p $(dir $@)
	${SolidPython_STL}

${OSC_PNG_DIR}/%.png: \
  ${OSC_PRINT_DIR}/%.py ${SolidPython} ${SpPythonEnvFile}
	mkdir -p ${OSC_BUILD_DIR}
	mkdir -p $(dir $@)
	${SolidPython_PNG}

# Assemblies.
${OSC_STL_DIR}/%.stl: \
  ${OSC_ASM_DIR}/%.py ${SolidPython} ${SpPythonEnvFile}
	mkdir -p ${OSC_BUILD_DIR}
	mkdir -p $(dir $@)
	${SolidPython_STL}

${OSC_PNG_DIR}/%.png: \
  ${OSC_ASM_DIR}/%.py ${SolidPython} ${SpPythonEnvFile}
	mkdir -p ${OSC_BUILD_DIR}
	mkdir -p $(dir $@)
	${SolidPython_PNG}

# Generated docs.
${OSC_DOC_DIR}/%.md: \
  ${OSC_INC_DIR}/%.py ${SolidPython} ${SpPythonEnvFile}
	mkdir -p $(dir $@)
	${PDOC}

${OSC_DOC_DIR}/%.md: \
  ${OSC_DRAW_DIR}/%.py ${SolidPython} ${SpPythonEnvFile}
	mkdir -p $(dir $@)
	${PDOC}

${OSC_DOC_DIR}/%.md: \
  ${OSC_PART_DIR}/%.py ${SolidPython} ${SpPythonEnvFile}
	mkdir -p $(dir $@)
	${PDOC}

${OSC_DOC_DIR}/%.md: \
  ${OSC_PRINT_DIR}/%.py ${SolidPython} ${SpPythonEnvFile}
	mkdir -p $(dir $@)
	${PDOC}

${OSC_DOC_DIR}/%.md: \
  ${OSC_ASM_DIR}/%.py ${SolidPython} ${SpPythonEnvFile}
	mkdir -p $(dir $@)
	${PDOC}

endif
endif

ifeq (${MAKECMDGOALS},help-model)
define EdOscadUsage
Usage: "make MODEL=<model>|MODEL_DIR=<dir> [<override>...] [<target>]"

MODEL=<model> The model to build. This is used as a prefix to the directories
        where the model scripts are located and is required.
or
MODEL_DIR=<path> The location of the model directory. MODEL is ignored.
Possible targets:
    all     All assembly files are processed and .stl and .png files
            produced.
    stl     Only the stl files are generated.
    png     Only the png files are generated. Not available in WSL.
    gui     Run the OpenSCAD GUI for each of the assembly files.
	        Not available in WSL.
    init    Initialize a new model directory. This creates a git
            repository in the models directory.
    docs    Generate documentation from model files.
    clean   Remove the dependency files and the output files.
    help    Display this help message (default).
	help-model
            Display the model specific help.
    show-<variable>
            This is a special target which can be used to display
            any makefile variable and exit.
    <asmfile>.stl
            Use this target to process a single assembly file. NOTE: The
            ASM command line variable can also be used for this purpose.
Possible <override>:
MODELS_DIR=<path> The directory where multiple models reside. This is
        ignored if MODEL_DIR is used.
ASM=<asmprefix>	This is the assembly prefix. The assemblies directory is scanned
        for files having this prefix and only those files are processed.
<target> A single target.
endef
endif

endif
